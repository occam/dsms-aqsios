import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
job_path       = os.getcwd()
workloads_path    = "%s/scripts" % (scripts_path)
os.system("cp -r %s/* %s" % (workloads_path, job_path))
binary_path         = "%s/aqsios2.0/gen_client/gen_client" % (scripts_path)
configs_path         = "%s/configs" % (scripts_path)
log_path	= "%s/log" % (job_path)
shed_path	= "%s/shed" % (job_path)

object = Occam.load()

# Open object.json for command line options
# (config options and workload)
data = object.configuration("Aqsios2.0 Options")

# Path to config
config_path = "%s/hr-np" % (configs_path)

# Path to script for workload
workload_path = "script_const_1500_identical"

# Generate input
for k, v in data.items():
  if k == "config_file":
    config_path = "%s/%s" % (configs_path, str(v))
  if k == "script":
    workload_path = str(v)

# Form arguments
args = [binary_path,
        "-l", log_path,
		"-c", config_path,
		"-s", shed_path,
        workload_path]

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run HMMSim
Occam.report(command)
